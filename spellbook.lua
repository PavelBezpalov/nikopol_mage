nikopol_mage_spellbook = {
  Amplification = 236628,
  ArcaneBarrage = 44425,
  ArcaneBlast = 30451,
  ArcaneExplosion = 1449,
  ArcaneFamilliar = 205022,
  ArcaneIntellect = 1459,
  ArcaneMissileProc = 79683,
  ArcaneMissiles = 5143,
  ArcaneOrb = 153626,
  ArcanePower = 12042,
  Berserking = 26297,
  BlasterMasterPowerID = 215,
  BlasterMaster = 274598,
  BlazingBarrier = 235313,
  Blink = 1953,
  Blizzard = 190356,
  DragonsBreath = 31661,
  BrainFreeze = 190446,
  ChargedUp = 205032,
  ChronoShift = 235711,
  ClearcastingBuff = 263725,
  Combustion = 190319,
  CometStorm = 153595,
  ConeofCold = 120,
  Counterspell = 2139,
  DragonsBreath = 31661,
  Ebonbolt = 257537,
  Erosion = 205039,
  Evocation = 12051,
  FingersofFrost = 44544,
  Fireball = 133,
  FireBlast = 108853,
  Flamestrike = 2120,
  Flurry = 44614,
  Frostbolt = 116,
  FrostBomb = 112948,
  FrostNova = 122,
  FrozenOrb = 84714,
  GlacialSpike = 199786,
  HeatingUp = 48107,
  HotStreak = 48108,
  IceBarrier = 11426,
  IceBlock = 45438,
  IceLance = 30455,
  IceNova = 157997,
  IceWard = 205036,
  Icicles = 205473,
  IcyVeins = 12472,
  IncantersFlow = 1463,
  LivingBomb = 44457,
  ManaShield = 235463,
  MarkofAluneth = 224968,
  Meteor = 153561,
  MirrorImage = 55342,
  NetherTempest = 114923,
  Overpowered = 155147,
  PhoenixFlames = 257541,
  PrecenseofMind = 205025,
  PrismaticBarrier = 235450,
  Pyroblast = 11366,
  Pyroclasm = 269651,
  RayofFrost = 205021,
  RemoveCurse = 475,
  Resonance = 205028,
  RingofFrost = 113724,
  RuleofThrees = 264354,
  RuneofPower = 116011,
  Scorch = 2948,
  Shimmer = 212653,
  Slipstream = 236457,
  Slow = 31589,
  SpellSteal = 30449,
  SummonWaterElemental = 31687,
  Supernova = 157980,
  TemporalFlux = 234302,
  UnstableMagic = 157976,
  WintersReach = 273347,
  WintersReachOther = 237346,
  WordsofPower = 205035
}
