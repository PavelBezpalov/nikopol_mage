-- Fire Mage for 8.1 by Nikopol
-- Talents: 3231123
-- Left Alt(Combat) - Ice Block
-- Left Control(Resting) - Arcane Intellect if down then Pyroblast
-- Left Shift - Blazing Barrier
-- You will see overlay on screen informing you should cast evocation then you should take safe position and hold left control to start casting evocation.
local dark_addon = dark_interface
local IT = nikopol_mage_items
local IS = nikopol_mage_items_spells
local SB = nikopol_mage_spellbook
local combustion_rop_cutoff = 60

local function burn_phase_duration()
  return GetTime() - burn_phase_started_at
end

local function azerite(power_id)
  local selected = 0
  for _, item_slot in AzeriteUtil.EnumerateEquipedAzeriteEmpoweredItems() do
    if C_AzeriteEmpoweredItem.IsPowerSelected(item_slot, power_id) then
      selected = selected + 1
    end
  end
  return selected
end

local function gcd_remains()
  local gcd_start, gcd_duration = GetSpellCooldown(61304)
  if gcd_start > 0 then
    return (gcd_duration - (GetTime() - gcd_start)) or 0
  end
  return 0
end

function casting_remains()
  local name, _, _, _, end_time = UnitCastingInfo("player")
  if name then
    local remains = end_time / 1000 - GetTime()
    return remains
  end
  return 0
end

local function combat()
  if not player.alive then return end
  
  local function time_to_die()
    local damagers = group.count(function (unit)
        return unit.alive and UnitGroupRolesAssigned(unit) == 'DAMAGER'
      end)

    if damagers == 0 then
      damagers = 1
    end

    return target.health.actual / (20000 * damagers)
  end
  
  local function firestarter_active()
    return talent(1,1) and target.health.percent > 90
  end
  
  local function firestarter_remains()
    local damagers = group.count(function (unit)
      return unit.alive and UnitGroupRolesAssigned(unit) == 'DAMAGER'
    end)

    if damagers == 0 then
      damagers = 1
    end

    return (target.health.actual - UnitHealthMax('target') * 0.9) / (20000 * damagers)
  end
  
  if GetCVar("nameplateShowEnemies") == '0' then
    SetCVar("nameplateShowEnemies", 1)
  end

  local active_enemies = enemies.count(function (unit)
    return unit.alive and unit.enemy and unit.combat and unit.distance >= target.distance - 5 and unit.distance <= target.distance + 5 and target.distance <= 40
  end)

  macro('/cqs')
  
  if GetItemCooldown(5512) == 0 and player.health.effective < 30 then
    macro('/use Healthstone')
  end

  if modifier.lshift and castable(SB.BlazingBarrier) then
    return cast(SB.BlazingBarrier)
  end

  if modifier.lalt and castable(SB.IceBlock) then
    return cast(SB.IceBlock)
  end
  
  if player.spell(IS.SurgingWaters).current then return end

  if toggle('dispell', false) and castable(SB.RemoveCurse) and player.dispellable(SB.RemoveCurse) then
    return cast(SB.RemoveCurse, player)
  end

  if toggle('spellsteal', false) and target.castable(SB.SpellSteal) and target.dispellable(SB.SpellSteal) then
    return cast(SB.SpellSteal, target)
  end

  if toggle('auto_target', false) then
    local nearest_target = enemies.match(function (unit)
        return unit.alive and unit.combat and unit.distance <= 40
      end)

    if (not target.exists or target.distance > 40) and nearest_target and nearest_target.name then
      macro('/target ' .. nearest_target.name)
    end
  end

  if not (target.enemy and target.alive and target.distance <= 40) then return end
  
  local angelic_casting = enemies.match(function (unit)
    return unit.alive and unit.combat and unit.distance <= 40 and unit.spell(287419).current
  end)

  if castable(SB.Counterspell) and angelic_casting and angelic_casting.name then
    for i=1,6 do
      macro('/target ' .. angelic_casting.name)
      if target.guid == angelic_casting.guid then break end
    end
  end
  
  if target.spell(287419).current then
    cast(SB.Counterspell, target)
  end

  if toggle('interrupts', false) and target.interrupt(80) then
    if target.castable(SB.Counterspell) then
      cast(SB.Counterspell, target)
    end
  end
  
  if player.spell(SB.Pyroblast).current and player.buff(SB.Pyroclasm).down then
    stopcast()
  end
  
  if target.castable(SB.LivingBomb) and (active_enemies > 1 and player.buff(SB.Combustion).down and (player.spell(SB.Combustion).cooldown > player.spell(SB.LivingBomb).cooldown_duration or player.spell(SB.Combustion).cooldown == 0)) then
    return cast(SB.LivingBomb, target)
  end
  
  if castable(SB.Meteor) 
  and (azerite(SB.BlasterMasterPowerID) > 0 and player.buff(SB.HeatingUp).up and player.spell(SB.FireBlast).charges > 0 or azerite(SB.BlasterMasterPowerID) == 0) then
    return cast(SB.Meteor, 'ground')
  end
  
  if toggle('cooldowns', false) then
    if castable(SB.RuneofPower) and not player.moving
    and player.buff(SB.Combustion).down 
    and (player.spell(SB.Combustion).cooldown == 0 or player.spell(SB.RuneofPower).recharge_duration < player.spell(SB.Combustion).cooldown 
      or player.spell(SB.RuneofPower).recharge < player.spell(SB.Combustion).cooldown + 2 and player.spell(SB.RuneofPower).charges >= 1) then
      return cast(SB.RuneofPower)
    end
    
    if castable(SB.Combustion) and ((player.spell(SB.RuneofPower).current and player.spell(SB.RuneofPower).casting_remains < 0.4) or not talent(3,3)) then
      return cast(SB.Combustion)
    end
  
    if castable(SB.MirrorImage) and player.buff(SB.Combustion).down then
      return cast(SB.MirrorImage)
    end
  
    if castable(SB.Berserking) then
      cast(SB.Berserking)
    end
    
    local start, duration, enable = GetInventoryItemCooldown("player", 13)
    if enable == 1 and start == 0 and not (GetInventoryItemID("player", 13) == IT.TidestormCodex) then
      return macro('/use 13')
    end

    start, duration, enable = GetInventoryItemCooldown("player", 14)
    if enable == 1 and start == 0 and not (GetInventoryItemID("player", 14) == IT.TidestormCodex) then
      return macro('/use 14')
    end
  end
  
  if toggle('multitarget', false) and castable(SB.Flamestrike) and ((talent(6,1) and active_enemies > 2) or active_enemies > 6) and player.buff(SB.HotStreak).up then
    return cast(SB.Flamestrike, 'ground')
  end
  
  if target.castable(SB.Pyroblast) and player.buff(SB.HotStreak).up then
    return cast(SB.Pyroblast, target)
  end
  
  if target.castable(SB.FireBlast) 
  and azerite(SB.BlasterMasterPowerID) > 0 
  and ((player.buff(SB.HeatingUp).up and player.buff(SB.Combustion).down and player.spell(SB.Meteor).lastcast) or
    (player.buff(SB.Combustion).up and not player.spell(SB.FireBlast).lastcast and player.buff(SB.BlasterMaster).remains < 0.3)) then
    return cast(SB.FireBlast, target)
  end
  
  if target.castable(SB.FireBlast) 
  and player.buff(SB.HeatingUp).up
  and (azerite(SB.BlasterMasterPowerID) > 0 and player.spell(SB.Meteor).cooldown > lastgcd() or azerite(SB.BlasterMasterPowerID) == 0 or not talent(7,3))
  and (player.spell(SB.Pyroblast).current or player.spell(SB.Fireball).current or player.spell(SB.Scorch).current) 
  and ((player.spell(SB.FireBlast).full_recharge_time + player.spell(SB.FireBlast).recharge_duration) < player.spell(SB.Combustion).cooldown or not toggle('cooldowns', false)) then
    return cast(SB.FireBlast, target)
  end
  
  if target.castable(SB.FireBlast) 
  and azerite(SB.BlasterMasterPowerID) == 0 
  and player.buff(SB.HeatingUp).up 
  and player.buff(SB.Combustion).up then
    return cast(SB.FireBlast, target)
  end
  
  if target.castable(SB.PhoenixFlames) and player.buff(SB.HeatingUp).up then
    return cast(SB.PhoenixFlames, target)
  end
  
  if target.castable(SB.Pyroblast) and not player.moving and player.buff(SB.Combustion).down
  and player.buff(SB.Pyroclasm).up and player.spell(SB.Pyroblast).castingtime + 1 < player.buff(SB.Pyroclasm).remains
  and (player.buff(SB.RuneofPower).up or (player.spell(SB.RuneofPower).recharge + player.spell(SB.Pyroblast).castingtime + 1) > player.buff(SB.Pyroclasm).remains and player.spell(SB.Combustion).cooldown > player.buff(SB.Pyroclasm).remains) then
    return cast(SB.Pyroblast, target)
  end
  
  if target.castable(SB.Scorch) and player.buff(SB.Combustion).up and player.spell(SB.Scorch).castingtime < player.buff(SB.Combustion).remains then
    return cast(SB.Scorch, target)
  end
  
  if target.castable(SB.LivingBomb) and player.buff(SB.Combustion).remains < 1.5 and active_enemies > 1 then
    return cast(SB.LivingBomb, target)
  end
  
  if castable(SB.DragonsBreath)
    and player.buff(SB.Combustion).remains < 1.5 and player.buff(SB.Combustion).up
    and target.distance <= 12 then
    return cast(SB.DragonsBreath)
  end
  
  if player.spell(SB.Combustion).cooldown > 20 and player.buff(SB.Combustion).down then
    local start, duration, enable = GetInventoryItemCooldown("player", 13)
    if enable == 1 and start == 0 and GetInventoryItemID("player", 13) == IT.TidestormCodex then
      return macro('/use 13')
    end

    start, duration, enable = GetInventoryItemCooldown("player", 14)
    if enable == 1 and start == 0 and GetInventoryItemID("player", 14) == IT.TidestormCodex then
      return macro('/use 14')
    end
  end
  
  if target.castable(SB.Scorch) 
    and target.health.percent <= 30
    and talent(1,3) then
    return cast(SB.Scorch, target)
  end
  
  if castable(SB.DragonsBreath)
  and active_enemies > 1
  and target.distance <= 12 then
    return cast(SB.DragonsBreath)
  end
  
  if target.castable(SB.Fireball) and not player.moving then
    return cast(SB.Fireball, target)
  end
  
  if target.castable(SB.Scorch) then
    return cast(SB.Scorch, target)
  end
end

local function resting()
  if not player.alive then return end
  
  dark_addon.settings.store('_engine_turbo', true)

  if GetItemCooldown(5512) == 0 and player.health.effective < 30 then
    macro('/use Healthstone')
  end

  if toggle('dispell', false) and castable(SB.RemoveCurse) and player.dispellable(SB.RemoveCurse) then
    return cast(SB.RemoveCurse, player)
  end

  if modifier.lshift and castable(SB.BlazingBarrier) then
    return cast(SB.BlazingBarrier)
  end

  if modifier.lcontrol then
    if player.buff(SB.ArcaneIntellect).down then
      return cast(SB.ArcaneIntellect)
    end

    if target.castable(SB.Pyroblast) then
      return cast(SB.Pyroblast, target)
    end
  end
end

function interface()
  dark_addon.interface.buttons.add_toggle({
      name = 'dispell',
      label = 'Auto Dispell',
      on = {
        label = 'DSP',
        color = dark_addon.interface.color.green,
        color2 = dark_addon.interface.color.green
      },
      off = {
        label = 'dsp',
        color = dark_addon.interface.color.grey,
        color2 = dark_addon.interface.color.dark_grey
      }
    })
  dark_addon.interface.buttons.add_toggle({
      name = 'spellsteal',
      label = 'Auto Spellsteal',
      on = {
        label = 'SS',
        color = dark_addon.interface.color.green,
        color2 = dark_addon.interface.color.green
      },
      off = {
        label = 'ss',
        color = dark_addon.interface.color.grey,
        color2 = dark_addon.interface.color.dark_grey
      }
    })
  dark_addon.interface.buttons.add_toggle({
      name = 'auto_target',
      label = 'Auto Target',
      on = {
        label = 'AT',
        color = dark_addon.interface.color.green,
        color2 = dark_addon.interface.color.green
      },
      off = {
        label = 'at',
        color = dark_addon.interface.color.grey,
        color2 = dark_addon.interface.color.dark_grey
      }
    })
end

dark_addon.rotation.register({
    spec = dark_addon.rotation.classes.mage.fire,
    name = 'fire_nikopol',
    label = 'Fire by Nikopol',
    combat = combat,
    resting = resting,
    interface = interface
})
