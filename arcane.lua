-- Arcane Mage for 8.1 by Nikopol
-- Talents: 2032021
-- Left Alt(Combat) - Ice Block
-- Left Control(Resting) - Arcane Intellect if down then Mirror Image then Arcane Blast
-- Left Control(Combat) - allow casting evocation
-- Left Shift - Prismatic Barrier
-- You will see overlay on screen informing you should cast evocation then you should take safe position and hold left control to start casting evocation.
local dark_addon = dark_interface
local IT = nikopol_mage_items
local IS = nikopol_mage_items_spells
local SB = nikopol_mage_spellbook
local burn_phase = false
local average_burn_length = 0
local total_burns = 0
local burn_phase_started_at = 0
local conserve_mana = 60
local evocation_alert = false

local function burn_phase_duration()
  return GetTime() - burn_phase_started_at
end

local function evocation_overlay()
  if evocation_alert then
    SpellActivationOverlay_ShowOverlay(SpellActivationOverlayFrame, 12051, "TEXTURES\\SPELLACTIVATIONOVERLAYS\\Maelstrom_Weapon_2.BLP", "TOP", 2, 255, 255, 255, false, false)
  else
    SpellActivationOverlay_HideOverlays(SpellActivationOverlayFrame, 12051)
  end
end

local function gcd()
  if not player.alive then return end
  
  evocation_overlay()
  
  if GetItemCooldown(5512) == 0 and player.health.effective < 30 then
    macro('/use Healthstone')
  end
  
  if player.spell(SB.Evocation).current and player.power.mana.percent >= 85 then
    stopcast()
  end
 
  if toggle('interrupts', false) and target.interrupt(70) and target.castable(SB.Counterspell) then
    cast(SB.Counterspell, target)
  end
end

local function combat()
  if not player.alive then return end
  
  evocation_overlay()
  evocation_alert = false
  
  local function time_to_die()
    local damagers = group.count(function (unit)
      return unit.alive and UnitGroupRolesAssigned(unit) == 'DAMAGER'
    end)
    
    if damagers == 0 then
      damagers = 1
    end

    return target.health.actual / (20000 * damagers)
  end
  
  if GetCVar("nameplateShowEnemies") == '0' then
    SetCVar("nameplateShowEnemies", 1)
  end
  
  local enemies_in_combat_within_same_range_as_target = enemies.count(function (unit)
    return unit.alive and unit.combat and unit.distance == target.distance
  end)

  local enemies_in_combat_within_10_yd = enemies.count(function (unit)
    return unit.alive and unit.combat and unit.distance <= 10
  end)

  macro('/cqs')
    
  if GetItemCooldown(5512) == 0 and player.health.effective < 30 then
    macro('/use Healthstone')
  end
  
  if modifier.lshift and castable(SB.PrismaticBarrier) then
    return cast(SB.PrismaticBarrier)
  end
  
  if modifier.lalt and castable(SB.IceBlock) then
    return cast(SB.IceBlock)
  end
  
  if player.spell(SB.Evocation).current and player.power.mana.percent >= 85 then
    stopcast()
  end
  
  if player.channeling() then return end
  
  if toggle('dispell', false) and castable(SB.RemoveCurse) and player.dispellable(SB.RemoveCurse) then
    return cast(SB.RemoveCurse, player)
  end
  
  if toggle('spellsteal', false) and target.castable(SB.SpellSteal) and target.dispellable(SB.SpellSteal) then
    return cast(SB.SpellSteal, target)
  end
  
  if toggle('auto_target', false) then
    local nearest_target = enemies.match(function (unit)
      return unit.alive and unit.combat and unit.distance <= 40
    end)
    
    if (not target.exists or target.distance > 40) and nearest_target and nearest_target.name then
      macro('/target ' .. nearest_target.name)
    end
  end
  
  if not (target.enemy and target.alive and target.distance <= 40) then return end
  
  if toggle('interrupts', false) and target.interrupt(80) then
    if target.castable(SB.Counterspell) then
      cast(SB.Counterspell, target)
    end
  end
    
  local function burn()
    if burn_phase then
      if player.spell(SB.Evocation).lastcast and time_to_die() > average_burn_length and burn_phase_duration() > 0 then
        burn_phase = false
        return
      end
    else 
      total_burns = total_burns + 1
      burn_phase = true
      burn_phase_started_at = GetTime()
    end
       
    if castable(SB.ChargedUp) and player.power.arcanecharges.actual <= 1 then
      return cast(SB.ChargedUp)
    end
    
    if castable(SB.MirrorImage) then
      return cast(SB.MirrorImage)
    end
    
    if target.castable(SB.NetherTempest) and target.debuff(SB.NetherTempest).remains < 2 and player.power.arcanecharges.deficit == 0 and player.buff(SB.RuneofPower).down and player.buff(SB.ArcanePower).down then
      return cast(SB.NetherTempest, target)
    end
    
    if target.castable(SB.ArcaneBlast) 
      and player.buff(SB.RuleofThrees).up
      and talent(7,1)
      and (not toggle('multitarget', false) or enemies_in_combat_within_10_yd < 3) then
      return cast(SB.ArcaneBlast, target)
    end
    
    if castable(SB.RuneofPower) and player.buff(SB.ArcanePower).down and (player.power.mana.percent >= 50 or castable(SB.ArcanePower)) and player.power.arcanecharges.deficit == 0 then
      return cast(SB.RuneofPower)
    end
    
    if castable(SB.Berserking) then
      cast(SB.Berserking)
    end
    
    if castable(SB.ArcanePower) then
      return cast(SB.ArcanePower)
    end
    
    if player.buff(SB.ArcanePower).up or time_to_die() < player.spell(SB.ArcanePower).cooldown then
      local start, duration, enable = GetInventoryItemCooldown("player", 13)
      if enable == 1 and start == 0 and not (GetInventoryItemID("player", 13) == IT.TidestormCodex) then
        return macro('/use 13')
      end

      start, duration, enable = GetInventoryItemCooldown("player", 14)
      if enable == 1 and start == 0 and not (GetInventoryItemID("player", 14) == IT.TidestormCodex) then
        return macro('/use 14')
      end
    end
    
    if castable(SB.PrecenseofMind) and ((player.buff(SB.ArcanePower).remains <= (2 * player.spell(SB.ArcaneBlast).castingtime)) or (player.buff(SB.ArcanePower).remains <= (2 * player.spell(SB.ArcaneBlast).castingtime))) then
      cast(SB.PrecenseofMind)
    end
    
    if castable(SB.ArcaneOrb) and (player.power.arcanecharges.actual == 0 or (enemies_in_combat_within_same_range_as_target < 3 or (enemies_in_combat_within_same_range_as_target < 2 and talent(4,1)))) then
      return cast(SB.ArcaneOrb)
    end
    
    if target.castable(SB.ArcaneBarrage) and enemies_in_combat_within_same_range_as_target >= 3 and player.power.arcanecharges.deficit == 0 then
      return cast(SB.ArcaneBarrage, target)
    end
    
    if toggle('multitarget', false) and castable(SB.ArcaneExplosion) and enemies_in_combat_within_10_yd >= 3 then
      return cast(SB.ArcaneExplosion)
    end
    
    if target.castable(SB.ArcaneMissiles) and player.buff(SB.ClearcastingBuff).up and (not toggle('multitarget', false) or enemies_in_combat_within_10_yd < 3) and (talent(1,1) or player.buff(SB.ArcanePower).down) then
      return cast(SB.ArcaneMissiles, target)
    end
    
    local start, duration, enable = GetInventoryItemCooldown("player", 13)
    if enable == 1 and start == 0 and GetInventoryItemID("player", 13) == IT.TidestormCodex then
      return macro('/use 13')
    end

    start, duration, enable = GetInventoryItemCooldown("player", 14)
    if enable == 1 and start == 0 and GetInventoryItemID("player", 14) == IT.TidestormCodex then
      return macro('/use 14')
    end
    
    if target.castable(SB.ArcaneBlast) and (not toggle('multitarget', false) or enemies_in_combat_within_10_yd < 3) then
      return cast(SB.ArcaneBlast, target)
    end
    
    average_burn_length = (average_burn_length * total_burns - average_burn_length + burn_phase_duration()) / total_burns
    
    if castable(SB.Evocation) then
      if modifier.lcontrol then
        return cast(SB.Evocation)
      else
        evocation_alert = true
      end
    end
    
    if target.castable(SB.ArcaneBarrage) then
      return cast(SB.ArcaneBarrage, target)
    end
  end
  
  local function conserve()
    if castable(SB.MirrorImage) then
      return cast(SB.MirrorImage)
    end
    
    if castable(SB.ChargedUp) and player.power.arcanecharges.actual == 0 then
      return cast(SB.ChargedUp)
    end
    
    if target.castable(SB.NetherTempest) and target.debuff(SB.NetherTempest).remains < 2 and player.power.arcanecharges.deficit == 0 and player.buff(SB.RuneofPower).down and player.buff(SB.ArcanePower).down then
      return cast(SB.NetherTempest, target)
    end
    
    if castable(SB.ArcaneOrb) and player.power.arcanecharges.actual <= 2 and (player.spell(SB.ArcanePower).cooldown > 10 or enemies_in_combat_within_same_range_as_target <= 2) then
      return cast(SB.ArcaneOrb)
    end
    
    local start, duration, enable = GetInventoryItemCooldown("player", 13)
    if enable == 1 and start == 0 and GetInventoryItemID("player", 13) == IT.TidestormCodex then
      return macro('/use 13')
    end

    start, duration, enable = GetInventoryItemCooldown("player", 14)
    if enable == 1 and start == 0 and GetInventoryItemID("player", 14) == IT.TidestormCodex then
      return macro('/use 14')
    end
        
    if target.castable(SB.ArcaneBlast) and player.buff(SB.RuleofThrees).up and player.power.arcanecharges.actual > 3 and (not toggle('multitarget', false) or enemies_in_combat_within_10_yd < 3) then
      return cast(SB.ArcaneBlast, target)
    end
    
    if castable(SB.RuneofPower) and player.power.arcanecharges.deficit == 0 and ((player.spell(SB.RuneofPower).recharge <= player.spell(SB.RuneofPower).castingtime) or (player.spell(SB.RuneofPower).recharge <= player.spell(SB.ArcanePower).cooldown) or (time_to_die() <= player.spell(SB.ArcanePower).cooldown)) then
      return cast(SB.RuneofPower)
    end
    
    if target.castable(SB.ArcaneMissiles) and player.power.mana.percent <= 95 and player.buff(SB.ClearcastingBuff).up and (not toggle('multitarget', false) or enemies_in_combat_within_10_yd < 3) then
      return cast(SB.ArcaneMissiles, target)
    end
    
    if target.castable(SB.ArcaneBarrage) and ((player.power.arcanecharges.deficit == 0 and ((player.power.mana.percent <= conserve_mana) or (player.spell(SB.ArcanePower).cooldown > player.spell(SB.RuneofPower).recharge and player.power.mana.percent <= conserve_mana + 25))) or (talent(7,3) and player.spell(SB.ArcaneOrb).cooldown <= lastgcd() and player.spell(SB.ArcanePower).cooldown > 10) or player.power.mana.percent <= conserve_mana - 10) then
      return cast(SB.ArcaneBarrage, target)
    end
    
    if target.castable(SB.Supernova) and player.power.mana.percent <= 95 then
      return cast(SB.Supernova, target)
    end
    
    if toggle('multitarget', false) 
      and castable(SB.ArcaneExplosion)
      and enemies_in_combat_within_10_yd >= 3
      and (player.power.mana.percent >= conserve_mana or player.power.arcanecharges.actual == 3) then
      return cast(SB.ArcaneExplosion)
    end
    
    if target.castable(SB.ArcaneBlast) then
      return cast(SB.ArcaneBlast, target)
    end
    
    if target.castable(SB.ArcaneBarrage) then
      return cast(SB.ArcaneBarrage, target)
    end
  end
  
  local function movement()
    if castable(SB.PrecenseofMind) then
      cast(SB.PrecenseofMind)
    end
    
    if castable(SB.ChargedUp) and player.power.arcanecharges.actual == 0 then
      return cast(SB.ChargedUp)
    end
    
    if target.castable(SB.NetherTempest) and target.debuff(SB.NetherTempest).remains < 2 and player.power.arcanecharges.deficit == 0 and player.buff(SB.RuneofPower).down and player.buff(SB.ArcanePower).down then
      return cast(SB.NetherTempest, target)
    end
    
    if toggle('multitarget', false) 
      and castable(SB.ArcaneExplosion)
      and enemies_in_combat_within_10_yd >= 3
      and (player.power.mana.percent >= conserve_mana or player.power.arcanecharges.actual == 3) then
      return cast(SB.ArcaneExplosion)
    end
    
    if castable(SB.ArcaneOrb) then
      return cast(SB.ArcaneOrb)
    end
    
    if target.castable(SB.Supernova) then
      return cast(SB.Supernova, target)
    end
    
    if target.castable(SB.ArcaneBlast) and player.buff(SB.PrecenseofMind).up then
      return cast(SB.ArcaneBlast, target)
    end
    
    if target.castable(SB.ArcaneBarrage) and player.power.arcanecharges.deficit == 0 then
      return cast(SB.ArcaneBarrage, target)
    end
    
    if castable(SB.PrismaticBarrier) and player.buff(SB.PrecenseofMind).down then
      return cast(SB.PrismaticBarrier)
    end
  end

  if player.moving then
    return movement()
  else
    if toggle('cooldowns', false) then
      if burn_phase 
        or (average_burn_length > 0 and time_to_die() < average_burn_length) then 
        return burn()
      end
      if castable(SB.ArcanePower) 
        and player.spell(SB.Evocation).cooldown <= average_burn_length
        and (player.power.arcanecharges.deficit == 0 or (castable(SB.ChargedUp) and player.power.arcanecharges.actual <= 1)) then
        return burn()
      end
    end

    if not burn_phase or not toggle('cooldowns', false) then return conserve() end
  end
end

local function resting()
  burn_phase = false
  average_burn_length = 0
  total_burns = 0
  burn_phase_started_at = 0
  evocation_alert = false
  
  if not player.alive then return end
  
  evocation_overlay()
  
  if GetItemCooldown(5512) == 0 and player.health.effective < 30 then
    macro('/use Healthstone')
  end
  
  if toggle('dispell', false) and castable(SB.RemoveCurse) and player.dispellable(SB.RemoveCurse) then
    return cast(SB.RemoveCurse, player)
  end
  
  if modifier.lshift and castable(SB.PrismaticBarrier) then
    return cast(SB.PrismaticBarrier)
  end
  
  if modifier.lcontrol then
    if player.buff(SB.ArcaneIntellect).down then
      return cast(SB.ArcaneIntellect)
    end
    
    if castable(SB.MirrorImage) then
      return cast(SB.MirrorImage)
    end
    
    if target.castable(SB.ArcaneBlast) then
      return cast(SB.ArcaneBlast, target)
    end
  end
end

function interface()
  dark_addon.interface.buttons.add_toggle({
    name = 'dispell',
    label = 'Auto Dispell',
    on = {
      label = 'DSP',
      color = dark_addon.interface.color.green,
      color2 = dark_addon.interface.color.green
    },
    off = {
      label = 'dsp',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })
  dark_addon.interface.buttons.add_toggle({
    name = 'spellsteal',
    label = 'Auto Spellsteal',
    on = {
      label = 'SS',
      color = dark_addon.interface.color.green,
      color2 = dark_addon.interface.color.green
    },
    off = {
      label = 'ss',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })
  dark_addon.interface.buttons.add_toggle({
    name = 'auto_target',
    label = 'Auto Target',
    on = {
      label = 'AT',
      color = dark_addon.interface.color.green,
      color2 = dark_addon.interface.color.green
    },
    off = {
      label = 'at',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })
end

dark_addon.rotation.register({
  spec = dark_addon.rotation.classes.mage.arcane,
  name = 'arcane_nikopol',
  label = 'Arcane by Nikopol',
  gcd = gcd,
  combat = combat,
  resting = resting,
  interface = interface
})
